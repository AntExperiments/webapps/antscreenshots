import { db as database } from 'config/db'
import { MongoClient } from 'mongodb'

const uri = database.url

const client = new MongoClient(uri)

export const db = async () => (await client.connect()).db(database.database)