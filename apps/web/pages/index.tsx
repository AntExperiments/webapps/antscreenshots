import { Button } from "../components"
import { db } from '../lib/mongo'
import { Shot } from '../../../packages/types'

import styles from '../styling/index.module.css'

export default function Web({ screenshots }) {
  console.log(screenshots)
  return (
    <div>
      <h1>Test</h1>
      <section className={styles.day}>
        {
          screenshots.map((shot: Shot) => <article key={shot.filenameHash}>
            {/* <Image src={`/api/thumbnail/${shot.filenameHash}`}></Image> */}
            <img src={`/api/thumbnail/${shot.filenameHash}`} alt="" />
          </article>)
        }
      </section>
    </div>
  );
}

export async function getServerSideProps() {
  const collection = (await db()).collection('screenshots')
  
  const data = (await collection.find({}).toArray())
  const screenshots = data.map(d => { d._id = null; return d as unknown as Shot })
  
  return { props: { screenshots } }
}