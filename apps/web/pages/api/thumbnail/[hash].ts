import fs from 'fs-extra'

export default function handler(req, res) {
    const { hash } = req.query
    const file = fs.readFileSync(`../../_thumbnails/${hash}.jpg`)
    res.end(file)
}
  