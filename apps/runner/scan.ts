import fs from 'fs-extra'
import { recognize } from "node-tesseract-ocr";
import imageThumbnail from 'image-thumbnail'
import { createHash } from 'crypto'
import fileExists from 'file-exists'
import { MongoClient } from 'mongodb'

import { thumbnailDir, imagesDir, db as database } from 'config'

const client = new MongoClient(database.url);

(async () => {
    const config: any = {
        responseType: "buffer",
        width: 300,
        height: 300,
        fit: "cover"
    }

    const files = await fs.readdir(imagesDir)

    //#region Database
    await client.connect()
    console.log('Connected successfully to Database')
    const db = client.db(database.database)
    const collection = db.collection('screenshots')
    //#endregion

    for (const key in files) {
        const file = `${imagesDir}/${files[key]}`
        const fileHash = createHash('md5').update(file).digest('hex')
        const thumbnail = `${thumbnailDir}/${fileHash}.jpg`

        // if file not yet exist
        if (!(await fileExists(thumbnail))) {
        console.log("Processing Thumbnail for:", files[key]);
        
            const thumbnailPromise = new Promise<string>(resolve => {

                imageThumbnail(file, config)
                .then((data: Buffer) => {
                    fs.writeFileSync(thumbnail, data)
                    resolve(fileHash)
                })
                .catch((err: any) => console.error(err))

            })

            const ocrPromise = new Promise<string>(resolve => {

                fs.readFile(file)
                .then(recognize)
                .then(resolve)

            })
        
            const [ filenameHash, ocr ] = await Promise.all([ thumbnailPromise, ocrPromise ])
            
            console.log(await collection.insertOne({
                filenameHash,
                ocr: ocr.replaceAll("\n", " ")
            }))

        }

    }

    console.log("done")
    client.close()

})()