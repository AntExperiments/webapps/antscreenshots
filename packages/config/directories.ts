import { resolve } from "path";
import { homedir as _homedir } from "os";

const getAbsolutePath = (filepath: string) => {
    const homedir = _homedir()
    return resolve(filepath.replace(/\~/g, homedir + "/"))
}

export const imagesDir = getAbsolutePath("../../_images")
export const thumbnailDir = getAbsolutePath("../../_thumbnails")